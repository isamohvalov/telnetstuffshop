package ru.spb.isamokhvalov.service;

import ru.spb.isamokhvalov.dto.Item;
import ru.spb.isamokhvalov.dto.UserInfo;
import ru.spb.isamokhvalov.exception.*;

import java.util.List;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 17:58
 */
public interface StuffShopService {

    public UserInfo login(String userName) throws UserAlreadyLoggedException, BadUserNameException;

    public void logout(String userName);

    public List<Item> viewShop();

    public UserInfo getUserInfo(String userName) throws BadUserNameException;

    public UserInfo buyItem(String userName, String itemId) throws ItemIsNotExistException, NoHaveMoneyException, BuyItemException, BadUserNameException;

    public UserInfo sellItem(String userName, String itemId) throws ItemIsNotExistException, BuyItemException, BadUserNameException;

}
