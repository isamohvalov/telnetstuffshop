package ru.spb.isamokhvalov.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.spb.isamokhvalov.dao.InventoryDao;
import ru.spb.isamokhvalov.dao.ShopDao;
import ru.spb.isamokhvalov.dao.UserDao;
import ru.spb.isamokhvalov.dto.Item;
import ru.spb.isamokhvalov.dto.UserInfo;
import ru.spb.isamokhvalov.exception.*;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 18:09
 */
@Component
public class StuffShopServiceImpl implements StuffShopService {

    private static final Object PRESENT = new Object();

    private Map<String, Object> usersSet = new ConcurrentHashMap<>();

    @Autowired
    UserDao userDao;

    @Autowired
    ShopDao shopDao;

    @Autowired
    InventoryDao inventoryDao;

    @Override
    public UserInfo login(String userName) throws UserAlreadyLoggedException, BadUserNameException {
        if (StringUtils.isBlank(userName))
            throw new BadUserNameException();
        if (usersSet.containsKey(userName))
            throw new UserAlreadyLoggedException();
        UserInfo userInfo = userDao.findUserByName(userName);
        if (userInfo == null) {
            userInfo = new UserInfo(userName, 100);
            userDao.saveNewUser(userInfo);
        }
        usersSet.put(userName, PRESENT);
        return userInfo;
    }

    @Override
    public void logout(String userName) {
        usersSet.remove(userName);
    }

    @Override
    public List<Item> viewShop() {
        return shopDao.getAllItems();
    }

    @Override
    public UserInfo getUserInfo(String userName) throws BadUserNameException {
        if (StringUtils.isBlank(userName))
            throw new BadUserNameException();
        UserInfo user = userDao.findUserByName(userName);
        user.getInventory().addAll(inventoryDao.getInventory(userName));
        return user;
    }

    @Override
    public UserInfo buyItem(String userName, String itemId) throws ItemIsNotExistException, NoHaveMoneyException, BuyItemException, BadUserNameException {
        Item item = findItemByIdOrName(itemId);

        UserInfo userInfo = userDao.findUserByName(userName);
        int balance = userInfo.getCash() - item.getCost();
        if (balance < 0)
            throw new NoHaveMoneyException();
        userInfo.setCash(balance);
        userDao.updateUserCash(userInfo);
        inventoryDao.addItemToInventory(userInfo.getUserName(), item.getId());
        return getUserInfo(userInfo.getUserName());
    }

    @Override
    public UserInfo sellItem(String userName, String itemId) throws ItemIsNotExistException, BuyItemException, BadUserNameException {
        Item item = findItemByIdOrName(itemId);
        UserInfo userInfo = userDao.findUserByName(userName);
        int balance = userInfo.getCash() + item.getCost();
        inventoryDao.removeItemFromInventory(userInfo.getUserName(), item.getId());
        userInfo.setCash(balance);
        userDao.updateUserCash(userInfo);
        return getUserInfo(userInfo.getUserName());
    }

    private Item findItemByIdOrName(String itemId) throws ItemIsNotExistException {
        Item item = null;
        try {
            item = shopDao.findItemById(Integer.parseInt(itemId));
        } catch (NumberFormatException ignored) {
            item = shopDao.findItemByName(itemId);
        }
        if (item == null)
            throw new ItemIsNotExistException();
        return item;
    }

}
