package ru.spb.isamokhvalov;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.spb.isamokhvalov.service.StuffShopService;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 13:19
 */
@Log4j2
public class ServerStart {

    public static void main(String[] args) throws Exception {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("ApplicationContext.xml");
        StuffShopService stuffShopService = ctx.getBean(StuffShopService.class);
        ServerSocket server = ctx.getBean(ServerSocket.class);
        log.info("Server started");
        while (true) {
            Socket client = server.accept();
            new TelnetServer(client, stuffShopService);
        }
    }
}
