package ru.spb.isamokhvalov.utils;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.bind.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 15:19
 */
public class XmlUtils {
    public static Node serializeRootObjectToNode(Object o) throws JAXBException, ParserConfigurationException {
        Class<?> c = o.getClass();
        if (o instanceof JAXBElement)
            c = ((JAXBElement) o).getDeclaredType();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document doc = dbf.newDocumentBuilder().newDocument();

        Marshaller marshaller = createMarshaller(c);
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(o, doc);

        return doc.getDocumentElement();
    }

    private static <J> Marshaller createMarshaller(Class<J> rootElementClass) throws JAXBException {
        JAXBContext context = getJaxbContaxt(rootElementClass);
        return context.createMarshaller();
    }
    public static JAXBContext getJaxbContaxt(Class o) throws JAXBException {
        if (!jaxbContexts.containsKey(o)) {
            jaxbContexts.putIfAbsent(o, JAXBContext.newInstance(o));
        }
        return jaxbContexts.get(o);
    }
    //простой кэш для jaxb контекстов
    private static ConcurrentMap<Class, JAXBContext> jaxbContexts = new ConcurrentHashMap<>();

    public static String serializeNodeToString(Node node) {
        StringWriter writer = new StringWriter();
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            DOMSource xmlSource = new DOMSource(node);
            StreamResult outputTarget = new StreamResult(writer);
            transformer.transform(xmlSource, outputTarget);

            return writer.toString();
        } catch (TransformerException e) {
            throw new RuntimeException(e);
        }
    }

    public static <J> J readNode(Node node, Class<J> rootElementClass) throws JAXBException {
        //skip null
        if (node == null) {
            return null;
        }

        Unmarshaller unmarshaller = createUnmarshaller(rootElementClass);

        //noinspection unchecked
        return (J) unmarshaller.unmarshal(node, rootElementClass).getValue();
    }

    private static <J> Unmarshaller createUnmarshaller(Class<J> rootElementClass) throws JAXBException {
        JAXBContext context = getJaxbContaxt(rootElementClass);
        return context.createUnmarshaller();
    }

    public static <J> J readXml(InputStream s, Class<J> rootElementClass) throws JAXBException {
        Unmarshaller unmarshaller = createUnmarshaller(rootElementClass);

        if (s == null) {
            throw new IllegalArgumentException("Stream can't be null");
        }

        //noinspection unchecked
        return (J) unmarshaller.unmarshal(new StreamSource(s), rootElementClass).getValue();
    }


}
