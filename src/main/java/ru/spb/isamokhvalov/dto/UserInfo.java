package ru.spb.isamokhvalov.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 14:00
 */
@Data
public class UserInfo {

    private String userName;
    private int cash;

    List<Item> inventory;

    public UserInfo(String userName, int cash) {
        this.userName = userName;
        this.cash = cash;
    }

    public List<Item> getInventory() {
        if (inventory == null)
            inventory = new ArrayList<>();
        return inventory;
    }

    @Override
    public String toString() {
        return "userName='" + userName + '\'' +
                ", cash=" + cash +
                ((getInventory().size() > 0) ? ", inventory=" + inventory : "");
    }
}
