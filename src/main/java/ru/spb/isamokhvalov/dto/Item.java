package ru.spb.isamokhvalov.dto;

import lombok.Data;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 17:23
 */
@Data
public class Item {

    private int id;
    private String name;
    private int cost;
    private String description;
    private String properties;

    public Item(int id, String name, int cost, String description, String properties) {
        this.id = id;
        this.name = name;
        this.cost = cost;
        this.description = description;
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "\n\tName: "+ name +
                "\n\tId: " + id + ", cost: " + cost +
                "\n\tDescription: " + description +
                "\n\tProperties: " + properties;
    }
}
