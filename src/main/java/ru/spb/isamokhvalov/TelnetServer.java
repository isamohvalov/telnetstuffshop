package ru.spb.isamokhvalov;

import lombok.extern.log4j.Log4j2;
import ru.spb.isamokhvalov.dto.Item;
import ru.spb.isamokhvalov.dto.UserInfo;
import ru.spb.isamokhvalov.exception.BadUserNameException;
import ru.spb.isamokhvalov.exception.ItemIsNotExistException;
import ru.spb.isamokhvalov.exception.NoHaveMoneyException;
import ru.spb.isamokhvalov.exception.UserAlreadyLoggedException;
import ru.spb.isamokhvalov.service.StuffShopService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.List;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 13:49
 */
@Log4j2
public class TelnetServer extends Thread {

    private final Socket clientSocket;
    private final StuffShopService service;
    private String userName;

    TelnetServer(Socket socket, StuffShopService stuffShopService) throws Exception {
        this.service = stuffShopService;
        this.clientSocket = socket;

        log.info("Client Connected ...");
        start();
    }

    private void setUserName(String userName) {
        this.userName = userName;
    }

    public void run() {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
             PrintStream writer = new PrintStream(clientSocket.getOutputStream(), true)) {
            boolean exit = false;
            String inputLine;
            String command;
            String argument;
            UserInfo user = null;
            while (!exit) {
                inputLine = reader.readLine();
                log.info("Received line: " + inputLine + ", from user: " + userName);
                String[] split = inputLine.split(" ");
                try {
                    if (split.length > 0) {
                        if (split.length > 1)
                            argument = split[1];
                        else
                            argument = null;
                        command = split[0];
                        switch (command) {
                            case "login":
                                user = service.login(argument);
                                setUserName(user.getUserName());
                                writer.println("Welcome, " + userName + "!");
                                break;
                            case "logout":
                                service.logout(userName);
                                writer.println("Bye, " + userName);
                                setUserName(null);
                                break;
                            case "view-shop":
                                List<Item> items = service.viewShop();
                                for (Item item : items)
                                    writer.println(item);
                                break;
                            case "my-info":
                                writer.println(service.getUserInfo(userName));
                                break;
                            case "buy":
                                user = service.buyItem(userName, argument);
                                writer.println(user);
                                break;
                            case "sell":
                                user = service.sellItem(userName, argument);
                                writer.println(user);
                                break;
                            case "quit":
                                if (userName != null)
                                    service.logout(userName);
                                exit = true;
                            default:
                                writer.println("Unknown command! Available command:\n" +
                                        "login <username>       : logging to server;\n" +
                                        "logout                 : logout from server;\n" +
                                        "view-shop              : view all items in the shop;\n" +
                                        "buy <itemId|itemName>  : buy item, copy to inventory;\n" +
                                        "sell <itemId|itemName> : sell itemm;\n" +
                                        "quit                   : logout and close session.");
                                break;
                        }
                    }
                } catch (BadUserNameException exception) {
                    writer.println("Bad user name!");
                } catch (UserAlreadyLoggedException exception) {
                    writer.println("User ith this name already logged!");
                } catch (NoHaveMoneyException exception) {
                    writer.println("No have money for buy item!");
                } catch (ItemIsNotExistException exception) {
                    writer.println("Can't find item by id");
                } catch (Exception ignored) {
                    log.error(ignored.getMessage(), ignored);
                }
            }
        } catch (Exception ignored) {
            log.error(ignored.getMessage(), ignored);
        } finally {
            try {
                if (clientSocket != null)
                    clientSocket.close();
            } catch (IOException ignored) {
                log.error(ignored.getMessage(), ignored);
            }

        }
    }

}

