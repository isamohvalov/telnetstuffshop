package ru.spb.isamokhvalov.dao;

import ru.spb.isamokhvalov.dto.Item;

import java.util.List;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 17:22
 */
public interface ShopDao {

    public List<Item> getAllItems();

    public Item findItemById(int itemId);

    public Item findItemByName(String itemName);
}
