package ru.spb.isamokhvalov.dao;

import generated.ItemDescription;
import generated.Items;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;
import ru.spb.isamokhvalov.dto.Item;
import ru.spb.isamokhvalov.utils.XmlUtils;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import javax.xml.bind.JAXBException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 17:25
 */
@DependsOn("myLiquibase")
@Component
@Log4j2
public class ShopDaoImpl implements ShopDao {

    @Autowired
    DataSource dataSource;

    @Value("${items.filePath}")
    String filePath;

    @PostConstruct
    public void updateShopItems() throws JAXBException, IOException {
        Items shopItems;
        if (StringUtils.isBlank(filePath))
            shopItems = XmlUtils.readXml(this.getClass().getClassLoader().getResourceAsStream("items.xml"), Items.class);
        else
            try (InputStream is = new FileInputStream(filePath)) {
                shopItems = XmlUtils.readXml(is, Items.class);
            }

        try (Connection connection = this.dataSource.getConnection();
             PreparedStatement insertStatement = connection.prepareStatement("insert into Items (id, name, cost, description, properties) values (?, ?, ?, ?, ?)");
             PreparedStatement validateOnExist = connection.prepareStatement("select count(*) from Items where id = ?");
             PreparedStatement updateStatement = connection.prepareStatement("update Items set name =?, cost =?, description =?, properties =? where id = ?")) {
            connection.setAutoCommit(false);
            for (ItemDescription item : shopItems.getItem()) {
                validateOnExist.setInt(1, item.getId());
                try (ResultSet resultSet = validateOnExist.executeQuery()) {
                    if (resultSet.next() & (resultSet.getInt(1) == 0)) {
                        insertStatement.setInt(1, item.getId());
                        insertStatement.setString(2, item.getName());
                        insertStatement.setInt(3, item.getCost());
                        insertStatement.setString(4, item.getDescription());
                        insertStatement.setString(5, "Properties");
                        insertStatement.executeUpdate();
                    } else {
                        updateStatement.setString(1, item.getName());
                        updateStatement.setInt(2, item.getCost());
                        updateStatement.setString(3, item.getDescription());
                        updateStatement.setString(4, "Properties");
                        updateStatement.setInt(5, item.getId());
                        updateStatement.executeUpdate();
                    }
                }
            }
            connection.commit();
        } catch (SQLException exception) {
            log.error(exception.getMessage(), exception);
        }
    }

    @Override
    public List<Item> getAllItems() {
        List<Item> result = new ArrayList<>();
        try (Connection connection = this.dataSource.getConnection();
             Statement preparedStatement = connection.createStatement()) {
            preparedStatement.execute("select * from Items order by id asc");
            try (ResultSet resultSet = preparedStatement.getResultSet()) {
                while (resultSet.next()) {
                    result.add(new Item(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5)));
                }
            }
        } catch (SQLException exception) {
            log.error(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public Item findItemById(int itemId) {
        Item result = null;
        try (Connection connection = this.dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("select * from Items where id = ?")) {
            preparedStatement.setInt(1, itemId);
            preparedStatement.execute();
            try (ResultSet resultSet = preparedStatement.getResultSet()) {
                while (resultSet.next()) {
                    result = new Item(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5));
                }
            }
        } catch (SQLException exception) {
            log.error(exception.getMessage(), exception);
        }
        return result;
    }

    @Override
    public Item findItemByName(String itemName) {
        Item result = null;
        try (Connection connection = this.dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("select * from Items where name = ?")) {
            preparedStatement.setString(1, itemName);
            preparedStatement.execute();
            try (ResultSet resultSet = preparedStatement.getResultSet()) {
                while (resultSet.next()) {
                    result = new Item(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5));
                }
            }
        } catch (SQLException exception) {
            log.error(exception.getMessage(), exception);
        }
        return result;
    }
}
