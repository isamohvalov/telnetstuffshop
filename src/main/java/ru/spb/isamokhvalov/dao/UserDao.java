package ru.spb.isamokhvalov.dao;

import ru.spb.isamokhvalov.dto.UserInfo;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 13:59
 */
public interface UserDao {

    public UserInfo findUserByName(String userName);

    public void saveNewUser(UserInfo userInfo);

    public void updateUserCash(UserInfo userInfo);

}
