package ru.spb.isamokhvalov.dao;

import ru.spb.isamokhvalov.dto.Item;
import ru.spb.isamokhvalov.exception.BuyItemException;

import java.util.List;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 19:42
 */
public interface InventoryDao {

    public void addItemToInventory(String userName, int itemId) throws BuyItemException;

    public void removeItemFromInventory(String userName, int itemId) throws BuyItemException;

    public List<Item> getInventory(String userName);
}
