package ru.spb.isamokhvalov.dao;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.spb.isamokhvalov.dto.UserInfo;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 17:01
 */
@Log4j2
@Component
public class UserDaoImpl implements UserDao {

    @Autowired
    DataSource dataSource;

    @Override
    public UserInfo findUserByName(String userName) {
        UserInfo userInfo = null;
        try (Connection connection = this.dataSource.getConnection();
             PreparedStatement cs = connection.prepareStatement("select * from users where username = ?")) {
            cs.setString(1, userName);
            cs.execute();

            try (ResultSet rs = cs.getResultSet()) {
                if (rs.next()) {
                    userInfo = new UserInfo(rs.getString(1), rs.getInt(2));
                }
            }
        } catch (SQLException exception) {
            log.error(exception.getMessage(), exception);
        }
        return userInfo;
    }

    @Override
    public void saveNewUser(UserInfo userInfo) {
        try (Connection connection = this.dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("insert into users(username, cash) values (?, ?)")) {
            preparedStatement.setString(1, userInfo.getUserName());
            preparedStatement.setInt(2, userInfo.getCash());
            preparedStatement.execute();
        } catch (SQLException exception) {
            log.error(exception.getMessage(), exception);
        }

    }

    @Override
    public void updateUserCash(UserInfo userInfo) {
        try (Connection connection = this.dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("update users set cash = ?")) {
            preparedStatement.setInt(1, userInfo.getCash());
            preparedStatement.execute();
        } catch (SQLException exception) {
            log.error(exception.getMessage(), exception);
        }
    }
}
