package ru.spb.isamokhvalov.dao;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.spb.isamokhvalov.dto.Item;
import ru.spb.isamokhvalov.exception.BuyItemException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 19:44
 */
@Log4j2
@Component
public class InventoryDaoImpl implements InventoryDao {

    @Autowired
    DataSource dataSource;

    public void addItemToInventory(String userName, int itemId) throws BuyItemException {
        try (Connection connection = this.dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("insert into Inventory (username, inventoryId) values (?, ?) ")) {
            preparedStatement.setString(1, userName);
            preparedStatement.setInt(2, itemId);
            if (preparedStatement.executeUpdate() != 1)
                throw new BuyItemException();
        } catch (SQLException exception) {
            log.error(exception.getMessage(), exception);
        }
    }

    public void removeItemFromInventory(String userName, int itemId) throws BuyItemException {
        try (Connection connection = this.dataSource.getConnection();
             PreparedStatement findStatement = connection.prepareStatement("select id from Inventory where username = ? and inventoryId = ?");
             PreparedStatement preparedStatement = connection.prepareStatement("delete from Inventory where id = ?")) {
            findStatement.setString(1, userName);
            findStatement.setInt(2, itemId);
            try (ResultSet resultSet = findStatement.executeQuery()) {
                if (resultSet.next()) {
                    preparedStatement.setInt(1, resultSet.getInt(1));
                    if (preparedStatement.executeUpdate() != 1)
                        throw new BuyItemException();
                } else
                    throw new BuyItemException();
            }
        } catch (SQLException exception) {
            log.error(exception.getMessage(), exception);
        }
    }

    public List<Item> getInventory(String userName) {
        List<Item> result = new ArrayList<>();
        try (Connection connection = this.dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement("select t1.* from items t1 join Inventory t2 on t2.inventoryId = t1.id where t2.username = ?")) {
            preparedStatement.setString(1, userName);
            preparedStatement.execute();
            try (ResultSet resultSet = preparedStatement.getResultSet()) {
                while (resultSet.next()) {
                    result.add(new Item(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5)));
                }
            }
        } catch (SQLException exception) {
            log.error(exception.getMessage(), exception);
        }
        return result;

    }
}
