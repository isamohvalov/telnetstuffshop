package ru.spb.isamokhvalov;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.spb.isamokhvalov.dao.InventoryDao;
import ru.spb.isamokhvalov.exception.BuyItemException;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 19:44
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:TestApplicationContext.xml"})
public class InventoryDaoTest {

    @Autowired
    InventoryDao inventoryDao;

    @Test
    public void testInventory() throws BuyItemException {
        final String spiderdemon = "spiderdemon";
        Assert.assertEquals(0, inventoryDao.getInventory(spiderdemon).size());
        inventoryDao.addItemToInventory(spiderdemon, 1);
        Assert.assertEquals(1, inventoryDao.getInventory(spiderdemon).size());
        inventoryDao.removeItemFromInventory(spiderdemon, 1);
        Assert.assertEquals(0, inventoryDao.getInventory(spiderdemon).size());
    }
}
