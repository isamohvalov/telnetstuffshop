package ru.spb.isamokhvalov;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.spb.isamokhvalov.dto.Item;
import ru.spb.isamokhvalov.dto.UserInfo;
import ru.spb.isamokhvalov.exception.*;
import ru.spb.isamokhvalov.service.StuffShopService;

import java.util.List;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 18:04
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:TestApplicationContext.xml"})
public class StuffShopServiceTest {

    @Autowired
    StuffShopService shopService;

    @Test
    public void testLoginSingleUser() throws UserAlreadyLoggedException, BadUserNameException {
        String cyberdemon = "cyberdemon";
        Assert.assertEquals(cyberdemon, shopService.login(cyberdemon).getUserName());
    }

    @Test(expected = BadUserNameException.class)
    public void testNullUser() throws UserAlreadyLoggedException, BadUserNameException {
        shopService.login(" ");
    }

    @Test(expected = UserAlreadyLoggedException.class)
    public void testUserAlreadyLoggedException() throws UserAlreadyLoggedException, BadUserNameException {
        String demon = "demon";
        shopService.login(demon);
        shopService.login(demon);
    }

    @Test
    public void testLogoutUser() throws UserAlreadyLoggedException, BadUserNameException {
        String creeper = "creeper";
        shopService.login(creeper);
        shopService.logout(creeper);
        shopService.login(creeper);
    }

    @Test
    public void testViewShop() {
        List<Item> items = shopService.viewShop();
        Assert.assertEquals(5, items.size());
        Assert.assertEquals("BFG9000", items.get(4).getName());
        Assert.assertEquals(100, items.get(2).getCost());
    }

    @Test
    public void testGetUserInfo() throws UserAlreadyLoggedException, BadUserNameException {
        String spectre = "spectre";
        shopService.login(spectre);
        UserInfo userInfo = shopService.getUserInfo(spectre);
        Assert.assertEquals(spectre, userInfo.getUserName());
        Assert.assertEquals(100, userInfo.getCash());
    }

    @Test
    public void testBuyItem() throws UserAlreadyLoggedException, NoHaveMoneyException, ItemIsNotExistException, BuyItemException, BadUserNameException {
        String cacodemon = "cacodemon";
        UserInfo login = shopService.login(cacodemon);
        login = shopService.buyItem(login.getUserName(), "1");
        Assert.assertEquals(1, login.getInventory().size());
        Assert.assertEquals(99, login.getCash());
    }

    @Test
    public void testSellItem() throws UserAlreadyLoggedException, BuyItemException, NoHaveMoneyException, ItemIsNotExistException, BadUserNameException {
        String zombieman = "zombieman";
        UserInfo login = shopService.login(zombieman);
        login = shopService.buyItem(login.getUserName(), "1");
        Assert.assertEquals(1, login.getInventory().size());
        Assert.assertEquals(99, login.getCash());
        login = shopService.buyItem(login.getUserName(), "Medikit");
        Assert.assertEquals(2, login.getInventory().size());
        Assert.assertEquals(49, login.getCash());

        login = shopService.sellItem(login.getUserName(), "Fist");
        Assert.assertEquals(1, login.getInventory().size());
        Assert.assertEquals(50, login.getCash());
        login = shopService.sellItem(login.getUserName(), "4");
        Assert.assertEquals(0, login.getInventory().size());
        Assert.assertEquals(100, login.getCash());
    }

    @Test(expected = NoHaveMoneyException.class)
    public void testNoMoneyToBuy() throws UserAlreadyLoggedException, BuyItemException, NoHaveMoneyException, ItemIsNotExistException, BadUserNameException {
        String user = "BaronOfTheHell";
        UserInfo login = shopService.login(user);
        shopService.buyItem(login.getUserName(), "BFG9000");
    }

    @Test
    public void testDeleteOneFromManyItems() throws UserAlreadyLoggedException, BadUserNameException, BuyItemException, NoHaveMoneyException, ItemIsNotExistException {
        final String imp = "imp";
        UserInfo user = shopService.login(imp);
        for (int i = 0; i < 5; i++)
            shopService.buyItem(user.getUserName(), "Fist");
        user = shopService.getUserInfo(user.getUserName());
        Assert.assertEquals(5, user.getInventory().size());
        user = shopService.sellItem(user.getUserName(), "Fist");
        Assert.assertEquals(4, user.getInventory().size());
    }


    @Test
    public void testManySells() throws UserAlreadyLoggedException, BadUserNameException {
        final String lostSoul = "lostSoul";
        UserInfo user = shopService.login(lostSoul);
        try {
            shopService.sellItem(user.getUserName(), "BFG9000");
        } catch (Exception e) {
            e.printStackTrace();
        }
        user = shopService.getUserInfo(user.getUserName());
        Assert.assertEquals(100, user.getCash());

    }
}
