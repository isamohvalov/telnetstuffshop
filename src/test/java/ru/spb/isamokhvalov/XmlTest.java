package ru.spb.isamokhvalov;

import generated.Items;
import org.junit.Assert;
import org.junit.Test;
import ru.spb.isamokhvalov.utils.XmlUtils;

import javax.xml.bind.JAXBException;

/**
 * User: isamokhvalov
 * Date: 07.08.16
 * Time: 15:21
 */
public class XmlTest {

//    @Test
//    public void testWriteXml() throws JAXBException, ParserConfigurationException {
//        Items items = new Items();
//        for (int i = 0; i < 5; i++) {
//            ItemDescription item = new ItemDescription();
//            item.setId(i + 1);
//            item.setCost((i + 3) * 10);
//            item.setName("Item № " + i);
//            Property prop = new Property();
//            prop.setName("Proper 1");
//            prop.setValue("Value 1");
//            Property prop1 = new Property();
//            prop1.setName("Proper 2");
//            prop1.setValue("Value 2");
//
//            item.getProperties().add(prop);
//            item.getProperties().add(prop1);
//            item.setDescription("The BFG is a fictional weapon found in many video game titles, mostly in first-person shooter series such as Doom and Quake.");
//            items.getItem().add(item);
//        }
//        System.out.println("items = " + XmlUtils.serializeNodeToString(XmlUtils.serializeRootObjectToNode(items)));
//    }

    @Test
    public void testReadXml() throws JAXBException {

        final Items items = XmlUtils.readXml(this.getClass().getClassLoader().getResourceAsStream("items.xml"), Items.class);
        Assert.assertEquals(5, items.getItem().size());
        Assert.assertEquals("BFG9000", items.getItem().get(4).getName());
        Assert.assertEquals(100, items.getItem().get(2).getCost());
    }
}
